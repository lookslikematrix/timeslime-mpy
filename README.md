# timeslime micropython

This project will make the functionality of [timeslime](https://gitlab.com/lookslikematrix/timeslime/) available with micropython on an ESP32.

## Getting started

* download repository and create own config with personal Wifi settings

    ~~~bash
    git clone https://gitlab.com/lookslikematrix/timeslime-mpy
    cd timeslime-mpy
    ~~~

* wire everything like here

    ![ESP32 wired](./assets/esp32.png)

* change settings in `timeslime.config` if necessary

|Key                  |Value                                                                    |Description                                                                                                 |
|---------------------|-------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
|CLK_PIN              |19                                                                       |You can set the CLK Pin for the TM1637 Display                                                              |
|DIO_PIN              |18                                                                       |You can set the DIO Pin for the TM1637 Display                                                              |
|LED_PIN              |23                                                                       |You can set the LED Pin                                                                                     |
|BUTTON_PIN           |22                                                                       |You can set the BUTTON Pin                                                                                  |
|access_point.SSID    |timeslime                                                                |Access point for *timeslime* configuration                                                                  |
|access_point.PSK     |timeslime                                                                |Password for this Access point                                                                              |
|access_point.dns_name|time.slime                                                               |DNS entry to access the configuration webserver                                                             |
|access_point.dns_ip  |192.168.4.1                                                              |IP Address for the DNS entry                                                                                |
|hostname             |timeslime                                                                |You can set the hostname for your timeslime                                                                 |

* install needed packages

    ~~~bash
    pipenv install -d
    pipenv shell
    ~~~

* install [timeslime-system](https://gitlab.com/lookslikematrix/timeslime-system) a micropython flavor on your board

    ~~~bash
    wget https://gitlab.com/lookslikematrix/timeslime-system/-/jobs/2330386830/artifacts/raw/timeslime-system_esp32_20220413_0b39652a.bin
    esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
    esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect -z 0x1000 timeslime-system_esp32_20220413_0b39652a.bin
    ~~~

* copy timeslime to board

    ~~~bash
    cd src/timeslime_mpy
    mpremote u0 cp -r . :
    ~~~

* unplug and plug your board, and *timeslime* should start now by opening an access point for configuration. Just connect with a device to the access point **timeslime** with password **timeslime**, open the browser on [http://time.slime](http://time.slime) and configure your *timeslime*

## Development

* install [timeslime-system](https://gitlab.com/lookslikematrix/timeslime-system) a micropython flavor on your board

    * on the ESP32 \[1\] download v1.1.0 on [timeslime-system_esp32_20220413_0b39652a.bin](https://gitlab.com/lookslikematrix/timeslime-system/-/jobs/2330386830/artifacts/raw/timeslime-system_esp32_20220413_0b39652a.bin)

        ~~~bash
        wget https://gitlab.com/lookslikematrix/timeslime-system/-/jobs/2330386830/artifacts/raw/timeslime-system_esp32_20220413_0b39652a.bin
        ~~~

    * find out which port your board is connected to your computer

        ~~~bash
        ls /dev/tty* > before_connected.txt
        ~~~

    * connect your board to the computer

        ~~~bash
        ls /dev/tty* > after_connected.txt
        diff before_connected.txt after_connected.txt
        # will print your port (/dev/ttyUSB0 in my case)
        ~~~

    * flash your board

        ~~~bash
        esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
        ~~~

    * install micropython on your board

        ~~~bash
        esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect -z 0x1000 timeslime-system_esp32_20220413_0b39652a.bin
        ~~~

* test with `mpremote` \[2\] if micropython is installed

    * connect to your board

        ~~~bash
        # u0 is a shortcut for /dev/ttyUSB0 (see documentation for more)
        mpremote u0
        ~~~

    * execute micropython code (e.g.)

        ~~~python
        print("timeslime")
        ~~~~

    * exit `mpremote` by pressing *Ctrl + ]*

* install `micropython` on your computer

    * install dependencies

        ~~~bash
        sudo apt install git build-essential libffi-dev -y
        ~~~

    * download the micropython repository and checkout v1.18 to use a stable version

        ~~~bash
        git clone https://github.com/micropython/micropython.git
        cd micropython
        git checkout v1.18
        cd ports/unix
        ~~~

    * compile micropython and mpy-cross

        ~~~bash
        make clean
        make submodules
        make
        make test
        cd ../../mpy-cross
        make
        ~~~

    * create a link to access `micropython` and `mpy-cross` from everywhere

        ~~~bash
        sudo ln -s $PWD/micropython /usr/local/bin/micropython
        sudo ln -s $PWD/mpy-cross /usr/local/bin/mpy-cross
        ~~~

* install needed modules. First go back to the *timeslime-mpy* repository

    ~~~bash
    cd timeslime-mpy/src
    micropython setup.py
    ~~~
* execute timeslime

    * copy files to board and execute script. This will create a valid database file.

        ~~~bash
        cd src/timeslime_mpy
        mpremote u0 mount . run main.py
        ~~~

* on the first start timeslime will start an access point. Connect to the configured SSID with a device and open http://time.slime

* after entering your settings and set them, timeslime generates two files

    1. *network.config*
        |Key  |Value         |Description                          |
        |-----|--------------|-------------------------------------|
        |SSID |WLANName      |You can set your SSID WLAN Name      |
        |PSK  |WLANPassword  |You can set your PSK WLAN Password   |

    2. *user.config*
        |Key             |Value                                                                         |Description                                                                                                 |
        |----------------|------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
        |weekly_hours    |['8:00:00', '8:00:00', '8:00:00', '8:00:00', '8:00:00', '0:00:00', '0:00:00'] |You can set your working time for each day in this array. Starting at monday.                               |
        |timeslime_server|http://192.168.178.10:8000/                                                   |You can run a [*timeslime-server*](https://gitlab.com/lookslikematrix/timeslime-server) to store your data. |
        |username        |test_user                                                                     |Your username for the *timeslime-server*.                                                                   |
        |password        |super_secret                                                                  |Your password for the *timeslime-server*.                                                                   |
        |utc_offset      |3600                                                                          |You can overwrite the UTC offset in seconds.                                                                |

* run tests

    ~~~bash
    cd src
    ./tests/run_tests.sh
    ~~~

## Links

\[1\]: [https://docs.micropython.org/en/latest/esp32/tutorial/intro.html](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html)

\[2\]: [https://pypi.org/project/mpremote/](https://pypi.org/project/mpremote/)
