# timeslime micropython

## 1.1

* make timeslime configurable via a webserver

* use [timeslime-system](https://gitlab.com/lookslikematrix/timeslime-system) instead of pure micropython with freezed modules

* make hostname configurable

* bugfix:

    * synchronize local changes with the server

    * set status of LED correct after synchronization

## 1.0

* implement same behavior like [timeslime](https://gitlab.com/lookslikematrix/timeslime) v1.5.0

* bugfix:

    * allow not to use [timeslime server](https://gitlab.com/lookslikematrix/timeslime-server)

    * ignore errors if there is a connection error to the timeslime server
