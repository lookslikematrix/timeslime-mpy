import btree

timespans_db_file = open("timespan.db", "r+b")
timespans_db = btree.open(timespans_db_file)

for guid in timespans_db:
     timespan_str = timespans_db[guid].decode("utf-8")
     print(timespan_str)
