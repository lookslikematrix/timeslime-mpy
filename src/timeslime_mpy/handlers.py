"""collection of handler classes"""
from array import array
import btree
from gc import collect
from os import remove
from ujson import dumps, loads
from datetime import datetime, timedelta, timezone
from tm1637 import TM1637
from ubinascii import b2a_base64
from urequests import request
import network
from microDNSSrv import MicroDNSSrv

from exceptions import ConnectionError
from models import Timespan, TimespanResponse
from serializers import TimespanResponseSerializer, TimespanSerializer


class TimeslimeHandler():
    """class for timeslime handler"""

    def __init__(
        self,
        get_recent_timespan: callable,
        get_tracked_time: callable,
        weekly_hours: array
        ):
        """initialize timeslime handler"""
        self.on_start = []
        self.on_started_by_sync = []
        self.on_stop = []
        self.on_stopped_by_sync = []
        if get_recent_timespan is None:
            self.timespan = Timespan()
        else:
            timespan = get_recent_timespan()
            if timespan is not None:
                self.timespan = timespan
            else:
                self.timespan = Timespan()
        self.get_tracked_time = get_tracked_time
        weekday = datetime.now(timezone.utc).weekday()
        (hours, minutes, seconds) = weekly_hours[weekday].split(":")
        self.daily_working_time = timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))

    def start_time(self, start_datetime = None):
        """start time"""
        self.stop_time()
        self.timespan = Timespan()
        if start_datetime is None or not isinstance(start_datetime, datetime):
            start_datetime = datetime.now(timezone.utc)
        self.timespan.start_time = start_datetime
        self.timespan.updated_at = datetime.now(timezone.utc)
        if len(self.on_start) > 0:
            try:
                for callback in self.on_start:
                    callback(self.timespan)
            except OSError:
                pass

    def stop_time(self, stop_datetime = None):
        """stop time"""
        if (self.timespan is not None and
            self.timespan.start_time is not None
            and self.timespan.stop_time is None):
            if stop_datetime is None or not isinstance(stop_datetime, datetime):
                stop_datetime = datetime.now(timezone.utc)
            self.timespan.stop_time = stop_datetime
            self.timespan.updated_at = datetime.now(timezone.utc)
        else:
            return
        if len(self.on_stop) > 0:
            try:
                for callback in self.on_stop:
                    callback(self.timespan)
            except OSError:
                pass

    def update(self, timespan: Timespan):
        """update timespan"""
        self.timespan = timespan
        if self.timespan is None:
            if len(self.on_stopped_by_sync) > 0:
                try:
                    for callback in self.on_stopped_by_sync:
                        callback(self.timespan)
                except OSError:
                    pass
            return

        if self.timespan is not None:
            if len(self.on_started_by_sync) > 0:
                try:
                    for callback in self.on_started_by_sync:
                        callback(self.timespan)
                except OSError:
                    pass

    def is_running(self):
        """return True if time is running"""
        if self.timespan is None:
            return False

        if self.timespan.start_time is not None and self.timespan.stop_time is None:
            return True

        return False

    def get_elapsed_time(self) -> timedelta:
        """get elapsed time"""
        tracked_time = self.get_tracked_time(datetime.now(timezone.utc))
        return self.daily_working_time - tracked_time

class DatabaseHandler():
    """class for database handler"""

    def __init__(self, utc_offset: int = 0, database = None, today: datetime = None):
        """initialize database handler"""
        self.utc_offset = utc_offset
        self.timespan_serializer = TimespanSerializer()
        if database is None:
            try:
                self.__timespans_db_file = open("timespan.db", "r+b")
            except OSError:
                self.__timespans_db_file = open("timespan.db", "w+b")
            self.timespans_db = btree.open(self.__timespans_db_file)
        else:
            self.timespans_db = database

        if today is None:
            today = datetime.now(timezone.utc)

        today += timedelta(seconds=self.utc_offset)
        today = datetime(today.year, today.month, today.day, tzinfo=timezone.utc)

        self.timespans = {}
        updated_database = False
        for guid in self.timespans_db:
            timespan_str = self.timespans_db[guid].decode("utf-8")
            timespan = self.timespan_serializer.deserialize(timespan_str)
            if today > timespan.start_time:
                del self.timespans_db[guid]
                updated_database = True
            else:
                self.timespans[guid] = timespan

        if updated_database and database is None:
            self.timespans_db.flush()

    def __del__(self):
        """destruct database handler"""
        self.timespans_db.close()
        self.__timespans_db_file.close()

    def save_timespan(self, timespan: Timespan):
        """save timespan in database"""
        id_bytes = bytes(str(timespan.id), "utf-8")
        self.timespans[id_bytes] = timespan
        timespan_str = self.timespan_serializer.serialize(timespan)
        self.timespans_db[id_bytes] = bytes(str(timespan_str), "utf-8")
        self.timespans_db.flush()

    def get_recent_timespan(self) -> Timespan:
        """get recent timespan"""
        for _, timespan in self.timespans.items():
            if timespan.stop_time is None:
                return timespan

        return None

    def get_tracked_time(self, now: datetime) -> timedelta:
        """get tracked time"""
        now_timezone = now + timedelta(seconds=self.utc_offset)
        tracked_time = timedelta()
        for _, timespan in self.timespans.items():
            if not(
                now_timezone.year == timespan.start_time.year and
                now_timezone.month == timespan.start_time.month and
                now_timezone.day == timespan.start_time.day
            ):
                continue

            if timespan.stop_time:
                tracked_time += timespan.stop_time - timespan.start_time
            else:
                tracked_time += now - timespan.start_time

        return tracked_time

class DisplayHandler():
    """class handling the display"""

    def __init__(self, display: TM1637):
        """initialize display handler"""
        self.hours = None
        self.minutes = None
        self.display = display

    def display_elapsed_time(self, timedelta: timedelta):
        """display elapsed time"""
        (hours, minutes) = self.__calculate_hours_minutes_from_total_seconds(timedelta)
        if self.hours != hours or self.minutes != minutes:
            self.hours = hours
            self.minutes = minutes
            self.display.numbers(self.hours, self.minutes)

    def __calculate_hours_minutes_from_total_seconds(self, timedelta_to_calculate: timedelta):
        """calculate properties from total_seconds"""
        total_seconds_to_calculate = timedelta_to_calculate.total_seconds()
        if total_seconds_to_calculate < 0:
            total_seconds_to_calculate = abs(total_seconds_to_calculate)

        minutes = 0
        hours = 0
        if total_seconds_to_calculate >= 60:
            minutes = total_seconds_to_calculate // 60

        if minutes >= 60:
            hours = minutes // 60
            minutes = minutes % 60

        return (int(hours), int(minutes))

class TimeslimeServerHandler():
    """class handling timeslime server"""

    def __init__(self, server_url, username, password):
        """initialize timeslime server handler"""
        if server_url is None or not server_url or server_url.isspace():
            raise ValueError

        self.server_url = server_url
        self.timespan_route = self.server_url + "api/v1/timespans"
        self.setting_route = self.server_url + "api/v1/settings"
        if username is None or not username or username.isspace():
            raise ValueError

        if password is None or not password or password.isspace():
            raise ValueError
        auth = "%s:%s" % (username, password)
        basic_auth = "Basic %s" % b2a_base64(auth).decode("utf-8").replace('\n', '')
        self.headers = {
            "Authorization": basic_auth,
            "Content-Type": "application/json"
        }
        self.timespan_serializer = TimespanSerializer()
        self.timespan_response_serializer = TimespanResponseSerializer()

    def __set_cookie(self, headers):
        if "Set-Cookie" in headers:
            self.headers["Cookie"] = headers["Set-Cookie"]

    def get_timespans(self, today: datetime, updated_at: datetime) -> TimespanResponse:
        """send a GET request to get timespans"""
        if today is None or not isinstance(today, datetime):
            raise TypeError

        today_uri_encoded = str(today).replace(" ", "%20").replace("+", "%2B")
        timespan_request = f"{self.timespan_route}?from={today_uri_encoded}"
        if updated_at is not None and isinstance(updated_at, datetime):
            updated_at_uri_encoded = str(updated_at).replace(" ", "%20").replace("+", "%2B")
            timespan_request += f"&updated_at={updated_at_uri_encoded}"
        response = request(
            "GET",
            timespan_request,
            headers=self.headers
        )
        if response.status_code != 200:
            raise OSError

        self.__set_cookie(response.headers)

        return self.timespan_response_serializer.deserialize(response.content.decode("utf-8"))

    def send_timespan(self, timespan: Timespan) -> Timespan:
        """send a POST request to create a timespan"""
        if timespan is None or timespan.start_time is None:
            raise TypeError

        data = self.timespan_serializer.serialize(timespan)
        try:
            response = request("POST", self.timespan_route, data=data, headers=self.headers)
            if response.status_code != 200:
                raise OSError

            self.__set_cookie(response.headers)

            return self.timespan_serializer.deserialize(response.content.decode("utf-8"))
        except OSError as os_error:
            if os_error.errno != 104:
                raise

class TimeslimeSyncHandler():
    """class handling timeslime synchronization"""

    def __init__(
        self,
        timeslime_server_handler: TimeslimeServerHandler,
        database_handler: DatabaseHandler,
        timeslime_handler: TimeslimeHandler) -> None:
        """initialize timeslime sync handler"""
        self.timeslime_server_handler = timeslime_server_handler
        self.database_handler = database_handler
        self.timeslime_handler = timeslime_handler
        self.last_sync_date = None

    def sync(self):
        """
        synchronize timeslime server with local database
        1. get all remote timespans from today
        2. iterate over all remote timespans and check if the remote timespan is newer than the local timespan. 
            If remote timespan == local timespan -> nothing
            If remote timespan > local timespan -> update local
            If remote timespan < local timespan -> send timespan to server
        3. check if there are local changes that aren't created on the server and send them
        4. update timeslime handler status
        """
        today = datetime.now(timezone.utc)
        today = datetime(today.year, today.month, today.day, tzinfo=timezone.utc)
        updated_at = None
        if self.last_sync_date is not None:
            updated_at = self.last_sync_date

        try:
            timespan_response = self.timeslime_server_handler.get_timespans(today, updated_at)
            local_changes = False
            for timespan in timespan_response.timespans:
                try:
                    current_timespan = self.database_handler.timespans[bytes(str(timespan.id), "utf-8")]
                    if (
                        timespan.updated_at == current_timespan.updated_at
                        ):
                        continue

                    if (
                        timespan.updated_at > current_timespan.updated_at
                        ):
                        self.database_handler.save_timespan(timespan)
                        local_changes = True
                    else:
                        self.timeslime_server_handler.send_timespan(current_timespan)
                except KeyError:
                    self.database_handler.save_timespan(timespan)
                    local_changes = True

            for _, timespan in self.database_handler.timespans.items():
                if timespan.created_at is None:
                    self.timeslime_server_handler.send_timespan(timespan)

            if local_changes:
                get_recent_timespan = self.database_handler.get_recent_timespan()
                self.timeslime_handler.update(get_recent_timespan)
            self.last_sync_date = timespan_response.request_time
        except MemoryError:
            collect()
        except OSError:
            pass


class NetworkHandler():
    """class handling network connections"""

    def __init__(self, hostname, file: str = "network.config"):
        """initialize network handler"""
        self.on_network_configuration_saved = []
        self.file = file
        self.hostname = hostname
        self.config = {}
        if not isinstance(self.file, str):
            self.config = self.file
        else:
            try:
                with open(self.file, "r", encoding="utf-8") as json_config:
                    self.config = loads(json_config.read())
            except OSError as os_error:
                if os_error.errno != 2:
                    raise

    def connect_wifi(self, timeout):
        """connect to wifi
        :param timeout: defines timeout in seconds"""
        ssid = self.config["SSID"]
        psk = self.config["PSK"]
        hostname = self.hostname
        try:
            wlan = network.WLAN(network.STA_IF)
            wlan.active(True)
            wlan.config(dhcp_hostname=hostname)
            if not wlan.isconnected():
                start_connection_time = datetime.now(timezone.utc)
                timeout_delta = timedelta(seconds=timeout)
                wlan = network.WLAN(network.STA_IF)
                wlan.active(True)
                wlan.connect(ssid, psk)
                while not wlan.isconnected():
                    delta = datetime.now(timezone.utc) - start_connection_time
                    if delta > timeout_delta:
                        raise ConnectionError(f"Reached timeout from {timeout} seconds.")
        except OSError as error:
            if error.errno == "Wifi Internal Error":
                raise ConnectionError(error.errno)

    def start_access_point(self, config):
        """start access point
        :param config: defines configuration"""
        ssid = config["access_point"]["SSID"]
        psk = config["access_point"]["PSK"]
        ap = network.WLAN(network.AP_IF)
        ap.active(True)
        ap.config(essid=ssid, password=psk)
        ap.config(authmode=3)
        ap.config(max_clients=1)
        MicroDNSSrv.Create({
            config["access_point"]["dns_name"] : config["access_point"]["dns_ip"]
        })

    def save_wifi_settings(self, config):
        """save wifi settings to file
        :param config: defines SSID and PSK
        :param file: defines network setting file (default network.config)"""
        if not ("SSID" in config and "PSK" in config):
            raise KeyError

        try:
            remove(self.file)
        except OSError as os_error:
            if os_error.errno != 2:
                raise

        file_stream = open(self.file, "w")
        file_stream.write(dumps(config))
        file_stream.close()
        if len(self.on_network_configuration_saved) > 0:
            for callback in self.on_network_configuration_saved:
                callback()


class UserSettingHandler():
    """class handling user settings"""

    def __init__(self, file: str = "user.config"):
        """initialize user setting handler"""
        self.file = file
        self.config = {}
        if not isinstance(self.file, str):
            self.config = self.file
        else:
            try:
                with open(self.file, "r", encoding="utf-8") as json_config:
                    self.config = loads(json_config.read())
            except OSError as os_error:
                if os_error.errno != 2:
                    raise

    def save_user_settings(self, config):
        """save user settings to file
        :param config: defines weekly_hours and utc_offset
        :param file: defines user setting file (default user.config)"""
        if not ("weekly_hours" in config and "utc_offset" in config):
            raise KeyError

        try:
            remove(self.file)
        except OSError as os_error:
            if os_error.errno != 2:
                raise

        file_stream = open(self.file, "w")
        file_stream.write(dumps(config))
        file_stream.close()
