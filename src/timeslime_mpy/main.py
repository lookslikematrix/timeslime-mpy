"""main timeslime script"""
from ujson import loads
from machine import reset, Pin, Timer
from time import sleep_ms, ticks_diff, ticks_ms
from tm1637 import TM1637

from exceptions import ConnectionError
from handlers import (
    NetworkHandler,
    UserSettingHandler
)

LAST = ticks_ms()

def button_handler(_, timeslime_handler):
    global LAST
    try:
        if ticks_diff(ticks_ms(), LAST) > 300:
            if timeslime_handler.is_running() is False:
                timeslime_handler.start_time()
            else:
                timeslime_handler.stop_time()
    finally:
        LAST = ticks_ms()

def restart(tm: TM1637):
    tm.show("RST5")
    sleep_ms(1000)
    tm.show("RST4")
    sleep_ms(1000)
    tm.show("RST3")
    sleep_ms(1000)
    tm.show("RST2")
    sleep_ms(1000)
    tm.show("RST1")
    sleep_ms(1000)
    tm.show("    ")
    reset()

def main():
    with open("timeslime.config", "r", encoding="utf-8") as json_config:
        config = loads(json_config.read())

    tm = TM1637(clk=Pin(config["CLK_PIN"]), dio=Pin(config["DIO_PIN"]), brightness=7)
    tm.show("INIT")

    led = Pin(config["LED_PIN"], Pin.OUT)
    led.off()

    network_handler = NetworkHandler(config["hostname"])
    network_handler.on_network_configuration_saved.append(lambda: restart(tm))

    user_setting_handler = UserSettingHandler()

    try:
        network_handler.connect_wifi(5)

        from ntptime import settime

        from handlers import (
            DatabaseHandler,
            DisplayHandler,
            TimeslimeHandler,
            TimeslimeServerHandler,
            TimeslimeSyncHandler
        )

        settime()

        user_config = user_setting_handler.config

        weekly_hours = user_config["weekly_hours"]

        database_handler = DatabaseHandler(user_config["utc_offset"])
        display_handler = DisplayHandler(tm)
        timeslime_server_enabled = False
        if "timeslime_server" in user_config and "username" in user_config and "password" in user_config:
            timeslime_server_enabled = True

        timeslime_handler = TimeslimeHandler(
            database_handler.get_recent_timespan,
            database_handler.get_tracked_time,
            weekly_hours
        )
        timeslime_handler.on_start.append(lambda _: led.on())
        timeslime_handler.on_start.append(database_handler.save_timespan)
        timeslime_handler.on_stop.append(lambda _: led.off())
        timeslime_handler.on_stop.append(database_handler.save_timespan)
        timeslime_handler.on_started_by_sync.append(lambda _: led.on())
        timeslime_handler.on_stopped_by_sync.append(lambda _: led.off())

        if timeslime_server_enabled:
            timeslime_server_handler = TimeslimeServerHandler(
                user_config["timeslime_server"],
                user_config["username"],
                user_config["password"]
            )
            timeslime_sync_handler = TimeslimeSyncHandler(
                timeslime_server_handler,
                database_handler,
                timeslime_handler
            )
            timeslime_sync_handler.sync()
            timeslime_handler.on_start.append(timeslime_server_handler.send_timespan)
            timeslime_handler.on_stop.append(timeslime_server_handler.send_timespan)

        if timeslime_handler.is_running():
            led.on()

        button = Pin(config["BUTTON_PIN"], Pin.IN, Pin.PULL_UP)
        button.irq(
            trigger = Pin.IRQ_RISING,
            handler = lambda pin: button_handler(pin, timeslime_handler)
        )

        tm.show("RDY ")

        display_timer = Timer(1)
        display_timer.init(period=1000, mode=Timer.PERIODIC, callback=lambda _: display_handler.display_elapsed_time(timeslime_handler.get_elapsed_time()))

        if timeslime_server_enabled:
            sync_timer = Timer(2)
            sync_timer.init(period=10000, mode=Timer.PERIODIC, callback=lambda _: timeslime_sync_handler.sync())

        while True:
            sleep_ms(60000)

    except (ConnectionError, KeyError, OSError):
        from webserver import WebServer

        tm.show("CONF")
        network_handler.start_access_point(config)

        web_server = WebServer(network_handler, user_setting_handler)
        web_server.start()

if __name__ == '__main__':
    main()
