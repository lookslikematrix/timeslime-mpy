"""webserver to configure timeslime"""
from datetime import timedelta
import tinyweb as tinyweb

from handlers import NetworkHandler, UserSettingHandler

class WebServer():
    """class for a webserver"""
    def __init__(self, network_handler: NetworkHandler, user_setting_handler: UserSettingHandler):
        """create webserver object"""
        self.network_handler = network_handler
        self.user_setting_handler = user_setting_handler
        self.app = tinyweb.webserver()

        self.app.add_route("/", self.index, methods=["GET"])
        self.app.add_route("/success", self.success, methods=["GET"])
        self.app.add_route(
            "/configure",
            self.configure,
            methods = ["POST"],
            max_body_size = 2048,
            save_headers = ["Content-Length", "Content-Type"],
            allowed_access_control_headers = ["Content-Length", "Content-Type"]
        )

    async def index(self, _, response):
        """index route"""
        await response.send_file("static/index.html")

    async def success(self, _, response):
        """success route"""
        await response.send_file("static/success.html")

    async def configure(self, request, response):
        """configure route"""
        data = await request.read_parse_form_data()
        network_settings = {
            "SSID": data["SSID"],
            "PSK": data["PSK"]
        }
        daily_hours = timedelta(seconds=(int(data["weekly_hours"]) * 60 * 60 / 5))
        weekly_hours_array = [
            str(daily_hours),
            str(daily_hours),
            str(daily_hours),
            str(daily_hours),
            str(daily_hours),
            str(timedelta()),
            str(timedelta()),
        ]

        user_settings = {
            "weekly_hours": weekly_hours_array,
            "utc_offset": int(data["utc_offset"]),
            "timeslime_server": data["timeslime_server"],
            "username": data["username"],
            "password": data["password"]
        }
        self.user_setting_handler.save_user_settings(user_settings)
        self.network_handler.save_wifi_settings(network_settings)
        await response.redirect("/success")

    def start(self):
        """start webserver"""
        self.app.run(host="0.0.0.0", port=80)