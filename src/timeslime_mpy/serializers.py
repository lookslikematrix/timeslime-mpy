"""collection of serializer"""
from ujson import dumps, loads
from datetime import datetime, timezone
from uuid import UUID

from models import Timespan, TimespanResponse

class TimespanSerializer():
    """class for timespan serializer"""

    @classmethod
    def serialize(cls, timespan: Timespan) -> str:
        """serialize timespan object to string
        param: timespan: defines timespan object"""
        if not isinstance(timespan, Timespan):
            raise ValueError

        start_time_str = None
        if timespan.start_time:
            start_time_str = str(timespan.start_time)

        stop_time_str = None
        if timespan.stop_time:
            stop_time_str = str(timespan.stop_time)

        created_at_str = None
        if timespan.created_at:
            created_at_str = str(timespan.created_at)

        updated_at_str = None
        if timespan.updated_at:
            updated_at_str = str(timespan.updated_at)

        return dumps({
            "id": timespan.id.hex,
            "start_time": start_time_str,
            "stop_time": stop_time_str,
            "created_at": created_at_str,
            "updated_at": updated_at_str
        })

    @classmethod
    def deserialize(cls, json_string: str) -> Timespan:
        """deserialize json to timespan object
        param: json_string: defines json"""
        if json_string is None:
            raise ValueError

        if isinstance(json_string, str):
            timespan_object = loads(json_string)
        else:
            timespan_object = json_string

        timespan_id = timespan_object["id"].replace("-", "")
        if len(timespan_id) != 32:
            raise ValueError("invalid id")

        timespan = Timespan()
        hex_string = "0x" + timespan_id
        id_bytes = int(hex_string, 16).to_bytes(16, "big")
        timespan.id = UUID(id_bytes)

        if timespan_object["start_time"] and timespan_object["start_time"] != "None":
            timespan.start_time = datetime.fromisoformat(timespan_object["start_time"]).replace(tzinfo=timezone.utc)
        if timespan_object["stop_time"] and timespan_object["stop_time"] != "None":
            timespan.stop_time = datetime.fromisoformat(timespan_object["stop_time"]).replace(tzinfo=timezone.utc)
        if (
            "created_at" in timespan_object and
            timespan_object["created_at"] and
            timespan_object["created_at"] != "None"
            ):
            timespan.created_at = datetime.fromisoformat(timespan_object["created_at"]).replace(tzinfo=timezone.utc)
        if (
            "updated_at" in timespan_object and
            timespan_object["updated_at"] and
            timespan_object["updated_at"] != "None"
            ):
            timespan.updated_at = datetime.fromisoformat(timespan_object["updated_at"]).replace(tzinfo=timezone.utc)

        return timespan

class TimespanResponseSerializer():
    """class for timespan response serialization"""

    @classmethod
    def deserialize(self, json_string: str) -> TimespanResponse:
        """deserialize json to timespan response object
        param: json_string: defines json"""
        if json_string is None:
            raise ValueError

        if isinstance(json_string, str):
            timespan_response_object = loads(json_string)
        else:
            timespan_response_object = json_string

        timespan_response = TimespanResponse()
        if timespan_response_object["request_time"] and timespan_response_object["request_time"] != "None":
            timespan_response.request_time = datetime.fromisoformat(timespan_response_object["request_time"]).replace(tzinfo=timezone.utc)

        if timespan_response_object["data"] and timespan_response_object["data"] != "None":
            for timespan_object in timespan_response_object["data"]:
                timespan_response.timespans.append(TimespanSerializer.deserialize(timespan_object))

        return timespan_response
