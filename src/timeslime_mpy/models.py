"""collection of models"""
from datetime import datetime
from uuid import uuid4

class Timespan():
    """class for timespan model"""

    def __init__(self):
        """initialize a timespan object"""
        self.id = uuid4()
        self.start_time = None
        self.stop_time = None
        self.created_at = None
        self.updated_at = None


class TimespanResponse:
    """timespan response model"""

    def __init__(self):
        """initialize a timespan response object"""
        self.timespans: list = []
        self.request_time: datetime
