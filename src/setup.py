import upip

packages = []

test_package = [
    "micropython-unittest==0.4"
]

upip.index_urls = ["https://pypi.org/pypi"]
upip.install(packages, "timeslime_mpy/modules")
upip.install(test_package, "tests/modules")
