from sys import path

from timeslime_mpy.models import Timespan
path.append("timeslime_mpy")

from gc import collect
import tests.modules.unittest as unittest

from datetime import datetime, timezone
from handlers import TimeslimeHandler

class TimeslimeHandlerTest(unittest.TestCase):
    """class for testing timeslime handler"""

    def test_start_time_current_time(self):
        """test start_time method"""
        # arrange
        timeslime_handler = TimeslimeHandler(
            None,
            None,
            ["8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00"]
        )

        # act
        timeslime_handler.start_time()

        # assert
        self.assertIsNotNone(timeslime_handler.timespan.start_time)
        self.assertTrue(isinstance(timeslime_handler.timespan.start_time, datetime))
        self.assertIsNotNone(timeslime_handler.timespan.updated_at)
        self.assertTrue(isinstance(timeslime_handler.timespan.updated_at, datetime))

    def test_start_time_defined_time(self):
        """test start_time method"""
        # arrange
        timeslime_handler = TimeslimeHandler(
            None,
            None,
            ["8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00"]
        )
        date = datetime(2022, 3, 6)

        # act
        timeslime_handler.start_time(date)

        # assert
        self.assertIsNotNone(timeslime_handler.timespan.start_time)
        self.assertTrue(isinstance(timeslime_handler.timespan.start_time, datetime))
        self.assertEqual(timeslime_handler.timespan.start_time, date)
        self.assertIsNotNone(timeslime_handler.timespan.updated_at)
        self.assertTrue(isinstance(timeslime_handler.timespan.updated_at, datetime))
        self.assertNotEqual(timeslime_handler.timespan.updated_at, date)

    def test_stop_time_no_start_time_nothing(self):
        """test stop_time method"""
        # arrange
        timeslime_handler = TimeslimeHandler(
            None,
            None,
            ["8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00"]
        )

        # act
        timeslime_handler.stop_time()

        # assert
        self.assertIsNotNone(timeslime_handler.timespan)
        self.assertIsNone(timeslime_handler.timespan.start_time)
        self.assertIsNone(timeslime_handler.timespan.stop_time)
        self.assertIsNone(timeslime_handler.timespan.updated_at)

    def test_stop_time_start_time_stopped_time(self):
        """test stop_time method"""
        # arrange
        timeslime_handler = TimeslimeHandler(
            None,
            None,
            ["8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00", "8:00:00"]
        )
        timeslime_handler.start_time()

        # act
        timeslime_handler.stop_time()

        # assert
        self.assertIsNotNone(timeslime_handler.timespan)
        self.assertIsNotNone(timeslime_handler.timespan.start_time)
        self.assertTrue(isinstance(timeslime_handler.timespan.start_time, datetime))
        self.assertIsNotNone(timeslime_handler.timespan.stop_time)
        self.assertTrue(isinstance(timeslime_handler.timespan.stop_time, datetime))
        self.assertIsNotNone(timeslime_handler.timespan.updated_at)
        self.assertTrue(isinstance(timeslime_handler.timespan.updated_at, datetime))


if __name__ == "__main__":
    collect()
    unittest.main()
