from sys import path
path.append("timeslime_mpy")

import btree
from datetime import datetime, timedelta, timezone
from gc import collect
from io import BytesIO
import tests.modules.unittest as unittest

from handlers import DatabaseHandler
from models import Timespan
from serializers import TimespanSerializer

class DatabaseHandlerTest(unittest.TestCase):
    """class for testing database handler"""

    def test_init_no_old_data(self):
        """test constructor"""
        # arrange & act
        database = btree.open(BytesIO())
        database_handler = DatabaseHandler(0, database)

        # assert
        with self.assertRaises(StopIteration):
            database_handler.timespans_db.__next__()

    def test_init_one_actual_dataset(self):
        """test constructor"""
        # arrange & act
        database = btree.open(BytesIO())
        timespan = Timespan()
        timespan.start_time = datetime.now(timezone.utc)
        id_bytes = bytes(str(timespan.id), "utf-8")
        timespan_serializer = TimespanSerializer()
        timespan_str = timespan_serializer.serialize(timespan)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")
        database_handler = DatabaseHandler(0, database)

        # assert
        self.assertEqual(len(database_handler.timespans), 1)

    def test_init_one_old_dataset_delete(self):
        """test constructor"""
        # arrange
        database = btree.open(BytesIO())
        timespan = Timespan()
        timespan.start_time = datetime(1991, 1, 12, tzinfo=timezone.utc)
        id_bytes = bytes(str(timespan.id), "utf-8")
        timespan_serializer = TimespanSerializer()
        timespan_str = timespan_serializer.serialize(timespan)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")

        # act
        database_handler = DatabaseHandler(0, database)

        # assert
        self.assertEqual(len(database_handler.timespans), 0)

    def test_init_one_old_dataset_at_utc_border_delete(self):
        """test constructor"""
        # arrange
        database = btree.open(BytesIO())
        timespan_1 = Timespan()
        timespan_1.start_time = datetime(1991, 1, 11, tzinfo=timezone.utc)
        id_bytes = bytes(str(timespan_1.id), "utf-8")
        timespan_serializer = TimespanSerializer()
        timespan_str = timespan_serializer.serialize(timespan_1)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")
        timespan_2 = Timespan()
        timespan_2.start_time = datetime(1991, 1, 12, tzinfo=timezone.utc)
        id_bytes = bytes(str(timespan_2.id), "utf-8")
        timespan_str = timespan_serializer.serialize(timespan_2)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")
        today = datetime(1991, 1, 11, hour=23, tzinfo=timezone.utc)

        # act
        database_handler = DatabaseHandler(3600, database, today)

        # assert
        self.assertEqual(len(database_handler.timespans), 1)
        self.assertEqual(str(database_handler.timespans[bytes(str(timespan_2.id), "utf-8")].id), str(timespan_2.id))

    def test_get_tracked_time_no_tracked_time_0(self):
        """test get_tracked_time"""
        # arrange
        database = btree.open(BytesIO())
        database_handler = DatabaseHandler(0, database)
        now = datetime(1991, 1, 12, tzinfo=timezone.utc)

        # act
        tracked_time = database_handler.get_tracked_time(now)

        # assert
        self.assertEqual(tracked_time, timedelta())

    def test_get_tracked_time_one_tracked_time_one_hour(self):
        """test get_tracked_time"""
        # arrange
        database = btree.open(BytesIO())
        timespan = Timespan()
        timespan.start_time = datetime(1991, 1, 12, hour=12, tzinfo=timezone.utc)
        timespan.stop_time = datetime(1991, 1, 12, hour=13, tzinfo=timezone.utc)
        id_bytes = bytes(str(timespan.id), "utf-8")
        timespan_serializer = TimespanSerializer()
        timespan_str = timespan_serializer.serialize(timespan)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")
        now = datetime(1991, 1, 12, tzinfo=timezone.utc)
        database_handler = DatabaseHandler(0, database, now)

        # act
        tracked_time = database_handler.get_tracked_time(now)

        # assert
        self.assertEqual(tracked_time, timedelta(hours=1))

    def test_get_tracked_time_utc_border_0(self):
        """test get_tracked_time"""
        # arrange
        database = btree.open(BytesIO())
        timespan = Timespan()
        timespan.start_time = datetime(1991, 1, 12, hour=12, tzinfo=timezone.utc)
        timespan.stop_time = datetime(1991, 1, 12, hour=13, tzinfo=timezone.utc)
        id_bytes = bytes(str(timespan.id), "utf-8")
        timespan_serializer = TimespanSerializer()
        timespan_str = timespan_serializer.serialize(timespan)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")
        today = datetime(1991, 1, 12, tzinfo=timezone.utc)
        database_handler = DatabaseHandler(3600, database, today)
        now = datetime(1991, 1, 12, hour=23, tzinfo=timezone.utc)

        # act
        tracked_time = database_handler.get_tracked_time(now)

        # assert
        self.assertEqual(tracked_time, timedelta())

    def test_get_tracked_time_utc_border_30_minutes(self):
        """test get_tracked_time"""
        # arrange
        database = btree.open(BytesIO())
        timespan = Timespan()
        timespan_serializer = TimespanSerializer()
        timespan.start_time = datetime(1991, 1, 12, hour=12, tzinfo=timezone.utc)
        timespan.stop_time = datetime(1991, 1, 12, hour=13, tzinfo=timezone.utc)
        id_bytes = bytes(str(timespan.id), "utf-8")
        timespan_str = timespan_serializer.serialize(timespan)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")
        timespan = Timespan()
        timespan.start_time = datetime(1991, 1, 13, hour=12, minute=30, tzinfo=timezone.utc)
        timespan.stop_time = datetime(1991, 1, 13, hour=13, tzinfo=timezone.utc)
        id_bytes = bytes(str(timespan.id), "utf-8")
        timespan_str = timespan_serializer.serialize(timespan)
        database[id_bytes] = bytes(str(timespan_str), "utf-8")
        today = datetime(1991, 1, 12, tzinfo=timezone.utc)
        database_handler = DatabaseHandler(3600, database, today)
        now = datetime(1991, 1, 12, hour=23, tzinfo=timezone.utc)

        # act
        tracked_time = database_handler.get_tracked_time(now)

        # assert
        self.assertEqual(tracked_time, timedelta(minutes=30))


if __name__ == "__main__":
    collect()
    unittest.main()
