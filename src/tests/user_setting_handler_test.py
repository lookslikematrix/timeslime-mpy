from sys import path
path.append("timeslime_mpy")

from gc import collect
from os import remove
from ujson import dumps
import tests.modules.unittest as unittest

from handlers import UserSettingHandler

class UserSettingHandlerTest(unittest.TestCase):
    """class for testing user setting handler"""

    def test_save_user_settings_invalid_data_error(self):
        """test save_user_settings method"""
        # arrange
        user_setting_handler = UserSettingHandler()
        config = {}

        # act & assert
        with self.assertRaises(KeyError):
            user_setting_handler.save_user_settings(config)

    def test_save_user_settings_missing_weekly_hours_error(self):
        """test save_user_settings method"""
        # arrange
        user_setting_handler = UserSettingHandler()
        config = {
            "utc_offset": 3600
        }

        # act & assert
        with self.assertRaises(KeyError):
            user_setting_handler.save_user_settings(config)

    def test_save_user_settings_valid_data_success(self):
        """test save_user_settings method"""
        # arrange
        file = "user.config.test"
        user_setting_handler = UserSettingHandler(file)
        config = {
            "weekly_hours": [
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "0:00:00",
                "0:00:00",
            ],
            "utc_offset": 3600
        }

        # act
        user_setting_handler.save_user_settings(config)

        # assert
        file_stream = open(file, "r")
        data = file_stream.read()
        self.assertEqual(data, dumps(config))
        remove(file)

    def test_save_user_settings_exists_already_overwrite(self):
        """test save_user_settings method"""
        # arrange
        file = "user.config.test"
        user_setting_handler = UserSettingHandler(file)
        config = {
            "weekly_hours": [
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "0:00:00",
                "0:00:00",
            ],
            "utc_offset": 3600
        }
        file_stream = open(file, "w")
        file_stream.write(dumps(config))
        file_stream.close()
        config_new = {
            "weekly_hours": [
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
                "8:00:00",
            ],
            "utc_offset": 7200
        }

        # act
        user_setting_handler.save_user_settings(config_new)

        # assert
        file_stream = open(file, "r")
        data = file_stream.read()
        self.assertEqual(data, dumps(config_new))
        remove(file)

if __name__ == "__main__":
    collect()
    unittest.main()
