from sys import path
path.append("timeslime_mpy")

from ujson import dumps

from gc import collect
import tests.modules.unittest as unittest

from datetime import datetime, timezone

from models import Timespan
from serializers import TimespanSerializer

class TimespanSeralizerTest(unittest.TestCase):
    """class for testing timespan serialization"""

    def test_serialize_none_error(self):
        """test serialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()

        # act & arrange
        with self.assertRaises(ValueError):
            timespan_serializer.serialize(None)

    def test_serialize_timespan_none_except_id(self):
        """test serialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()
        timespan = Timespan()

        # act
        json = timespan_serializer.serialize(timespan)

        # assert
        response_json = dumps({
            "id": timespan.id.hex,
            "stop_time": None,
            "start_time": None,
            "created_at": None,
            "updated_at": None
        })
        self.assertEqual(json, response_json)

    def test_serialize_timespan_success(self):
        """test serialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()
        timespan = Timespan()
        timespan.start_time = str(datetime(2021, 12, 3, hour=20, minute=15, second=5))
        timespan.stop_time = str(datetime(2021, 12, 3, hour=21, minute=6, second=5))
        timespan.created_at = str(datetime(2021, 12, 3, hour=21, minute=6, second=5))
        timespan.updated_at = str(datetime(2021, 12, 3, hour=21, minute=6, second=5))


        # act
        json = timespan_serializer.serialize(timespan)

        # assert
        response_json = dumps({
            "id": timespan.id.hex,
            "start_time": "2021-12-03 20:15:05",
            "stop_time": "2021-12-03 21:06:05",
            "created_at": "2021-12-03 21:06:05",
            "updated_at": "2021-12-03 21:06:05",
        })
        self.assertEqual(json, response_json)

    def test_deserialize_none_error(self):
        """test deserialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()

        # act & arrange
        with self.assertRaises(ValueError):
            timespan_serializer.deserialize(None)

    def test_deserialize_timespan_none_except_id(self):
        """test deserialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()
        json = {
            "id": "d23409dc240b4b34b0bd3b7b17e903d7",
            "stop_time": None,
            "start_time": None,
        }

        # act
        timespan = timespan_serializer.deserialize(json)

        # assert
        self.assertEqual("d23409dc240b4b34b0bd3b7b17e903d7", timespan.id.hex)
        self.assertEqual(None, timespan.start_time)
        self.assertEqual(None, timespan.stop_time)

    def test_deserialize_wrong_id_error(self):
        """test deserialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()
        json = {
            "id": "d23409dc240b4b34b0bd3b7b17e903",
            "stop_time": None,
            "start_time": None,
        }

        # act & assert
        with self.assertRaises(ValueError):
            timespan_serializer.deserialize(json)

    def test_deserialize_timespan_success(self):
        """test deserialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()
        json = {
            "id": "d23409dc240b4b34b0bd3b7b17e903d7",
            "start_time": "2021-12-03 20:15:59",
            "stop_time": "2021-12-03 21:05:59",
        }

        # act
        timespan = timespan_serializer.deserialize(json)

        # assert
        self.assertEqual("d23409dc240b4b34b0bd3b7b17e903d7", timespan.id.hex)
        self.assertEqual(datetime(2021, 12, 3, hour=20, minute=15, second=59, tzinfo=timezone.utc), timespan.start_time)
        self.assertEqual(datetime(2021, 12, 3, hour=21, minute=5, second=59, tzinfo=timezone.utc), timespan.stop_time)

    def test_deserialize_id_with_minus_timespan_success(self):
        """test deserialize method"""
        # arrange
        timespan_serializer = TimespanSerializer()
        json = {
            "id": "861ebd78-83e2-4e78-9e33-ba5a7a86ef3c",
            "start_time": "2021-12-03 20:15:59",
            "stop_time": "2021-12-03 21:05:59",
        }

        # act
        timespan = timespan_serializer.deserialize(json)

        # assert
        self.assertEqual("861ebd7883e24e789e33ba5a7a86ef3c", timespan.id.hex)
        self.assertEqual(datetime(2021, 12, 3, hour=20, minute=15, second=59, tzinfo=timezone.utc), timespan.start_time)
        self.assertEqual(datetime(2021, 12, 3, hour=21, minute=5, second=59, tzinfo=timezone.utc), timespan.stop_time)


if __name__ == "__main__":
    collect()
    unittest.main()
