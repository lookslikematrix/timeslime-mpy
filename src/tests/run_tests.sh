python3 -m pipenv run mpremote u0 cp -r . :

python3 -m pipenv run mpremote u0 run tests/timespan_serializer_test.py run tests/timespan_response_serializer_test.py

python3 -m pipenv run mpremote u0 run tests/database_handler_test.py

python3 -m pipenv run mpremote u0 run tests/timeslime_server_handler_test.py

python3 -m pipenv run mpremote u0 run tests/network_handler_test.py

python3 -m pipenv run mpremote u0 run tests/user_setting_handler_test.py

python3 -m pipenv run mpremote u0 run tests/timeslime_handler_test.py
