from sys import path
path.append("timeslime_mpy")

from gc import collect
import tests.modules.unittest as unittest

from datetime import datetime

from handlers import TimeslimeServerHandler

class TimeslimeServerHandlerTest(unittest.TestCase):
    """class for testing timeslime server handler"""

    def test_init_no_serverurl(self):
        """test constructor"""
        # arrange & act & assert
        with self.assertRaises(ValueError):
            TimeslimeServerHandler("", "test_user", "supersecret")

    def test_init_no_username(self):
        """test constructor"""
        # arrange & act & assert
        with self.assertRaises(ValueError):
            TimeslimeServerHandler("http://localhost:5000/", None, None)

    def test_init_no_password(self):
        """test constructor"""
        # arrange & act & assert
        with self.assertRaises(ValueError):
            TimeslimeServerHandler("http://localhost:5000/", "test_user", None)

    def test_init_user_correct_header(self):
        """test constructor"""
        # arrange & act
        timeslime_server_handler = TimeslimeServerHandler("http://localhost:5000/", "test_user", "supersecret")

        # assert
        self.assertEqual(timeslime_server_handler.headers, {
            "Authorization": "Basic dGVzdF91c2VyOnN1cGVyc2VjcmV0",
            "Content-Type": "application/json"
        })

    def test_get_timespans_no_date_error(self):
        # arrange
        timeslime_server_handler = TimeslimeServerHandler("http://localhost:5000/", "test_user", "supersecret")

        # act & assert
        with self.assertRaises(TypeError):
            timeslime_server_handler.get_timespans(None, None)

    def test_get_timespans_wrong_server_error(self):
        # arrange
        timeslime_server_handler = TimeslimeServerHandler("http://localhost:5000/", "test_user", "supersecret")

        # act & assert
        with self.assertRaises(OSError):
            timeslime_server_handler.get_timespans(datetime(2022, 3, 6), None)


if __name__ == "__main__":
    collect()
    unittest.main()
