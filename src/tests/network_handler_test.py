from sys import path
path.append("timeslime_mpy")

from gc import collect
from ujson import loads
from os import remove
import tests.modules.unittest as unittest

from handlers import ConnectionError, NetworkHandler

class NetworkHandlerTest(unittest.TestCase):
    """class for testing network handler"""

    def test_connect_wifi_no_config_key_error(self):
        """test connect_wifi method"""
        # arrange
        config = {}
        network_handler = NetworkHandler("timeslime", config)

        # act & assert
        with self.assertRaises(KeyError):
            network_handler.connect_wifi(0)

    def test_connect_wifi_missing_config_key_error(self):
        """test connect_wifi method"""
        # arrange
        config = {
            "SSID": "time"
        }
        network_handler = NetworkHandler("timeslime", config)

        # act & assert
        with self.assertRaises(KeyError):
            network_handler.connect_wifi(0)

    def test_connect_wifi_wrong_wifi_timeout(self):
        """test connect_wifi method"""
        # arrange
        config = {
            "SSID": "time",
            "PSK": "slime"
        }
        network_handler = NetworkHandler("timeslime", config)

        # act & assert
        try:
            network_handler.connect_wifi(0)
            self.assertTrue(False)
        except Exception as exception:
            self.assertTrue(isinstance(exception, ConnectionError))
            self.assertEqual(str(exception), "Reached timeout from 0 seconds.")

    def test_connect_wifi_wrong_wifi_5_timeout(self):
        """test connect_wifi method"""
        # arrange
        config = {
            "SSID": "time",
            "PSK": "slime"
        }
        network_handler = NetworkHandler("timeslime", config)

        # act & assert
        try:
            network_handler.connect_wifi(5)
            self.assertTrue(False)
        except Exception as exception:
            self.assertTrue(isinstance(exception, ConnectionError))
            self.assertEqual(str(exception), "Wifi Internal Error")

    def test_start_access_point_missing_config_error(self):
        """test start_access_point method"""
        # arrange
        network_handler = NetworkHandler("timeslime")
        config = {}

        # act & assert
        with self.assertRaises(KeyError):
            network_handler.start_access_point(config)

    def test_start_access_point_missing_sub_config_error(self):
        """test start_access_point method"""
        # arrange
        network_handler = NetworkHandler("timeslime")
        config = {
            "access_point": {
                "SSID": "timeslime"
            }
        }

        # act & assert
        with self.assertRaises(KeyError):
            network_handler.start_access_point(config)

    def test_start_access_point_correct_config_success(self):
        """test start_access_point method"""
        # arrange
        network_handler = NetworkHandler("timeslime")
        config = {
            "access_point": {
                "SSID": "timeslime",
                "PSK": "timeslime",
                "dns_name": "time.slime",
                "dns_ip": "192.168.4.1"
            }
        }

        # act
        network_handler.start_access_point(config)
        self.assertTrue(True)

    def test_save_wifi_settings_invalid_data_error(self):
        """test save_wifi_settings method"""
        # arrange
        network_handler = NetworkHandler("timeslime")
        config = {}

        # act & assert
        with self.assertRaises(KeyError):
            network_handler.save_wifi_settings(config)

    def test_save_wifi_settings_missing_psk_error(self):
        """test save_wifi_settings method"""
        # arrange
        network_handler = NetworkHandler("timeslime")
        config = {
            "SSID": "test_ssid"
        }

        # act & assert
        with self.assertRaises(KeyError):
            network_handler.save_wifi_settings(config)

    def test_save_wifi_settings_valid_data_success(self):
        """test save_wifi_settings method"""
        # arrange
        file = "network.config.test"
        network_handler = NetworkHandler("timeslime", file)
        config = {
            "SSID": "test_ssid",
            "PSK": "test_psk"
        }

        # act
        network_handler.save_wifi_settings(config)

        # assert
        file_stream = open(file, "r")
        data = loads(file_stream.read())
        self.assertEqual(data, config)
        remove(file)

    def test_save_wifi_settings_exists_already_overwrite(self):
        """test save_wifi_settings method"""
        # arrange
        file = "network.config.test"
        network_handler = NetworkHandler("timeslime", file)
        config = {
            "SSID": "test_ssid",
            "PSK": "test_psk"
        }
        file_stream = open(file, "w")
        file_stream.write(str(config))
        file_stream.close()
        config_new = {
            "SSID": "test_ssid",
            "PSK": "test_psk_new"
        }

        # act
        network_handler.save_wifi_settings(config_new)

        # assert
        file_stream = open(file, "r")
        data = loads(file_stream.read())
        self.assertEqual(data, config_new)
        remove(file)

if __name__ == "__main__":
    collect()
    unittest.main()
