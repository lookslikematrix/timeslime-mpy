from sys import path
path.append("timeslime_mpy")

from gc import collect
import tests.modules.unittest as unittest

from datetime import datetime, timezone

from serializers import TimespanResponseSerializer

class TimespanResponseSerializerTest(unittest.TestCase):
    """class for testing timespan response serialization"""

    def test_deserialize_none_error(self):
        """test deserialize method"""
        # arrange
        timespan_response_serializer = TimespanResponseSerializer()

        # act & arrange
        with self.assertRaises(ValueError):
            timespan_response_serializer.deserialize(None)

    def test_deserialize_timespan_response(self):
        """test deserialize method"""
        # arrange
        timespan_response_serializer = TimespanResponseSerializer()
        json = {
            'request_time': '2022-01-24 14:28:32.872532',
            'data': [
                {
                    'updated_at': '2022-01-24 13:34:37.823251',
                    'start_time': '2022-01-24 13:34:28',
                    'stop_time': '2022-01-24 13:34:35',
                    'id': '5bacb7f1-16e0-4db2-b9aa-e07bbf9d6493',
                    'created_at': '2022-01-24 13:34:30.676959'
                },
                {
                    'updated_at': '2022-01-24 13:51:44.357371',
                    'start_time': '2022-01-24 13:48:31',
                    'stop_time': '2022-01-24 13:51:42', 'id':
                    'abe433c5-724c-442e-b3cd-3ded93b0563e',
                    'created_at': '2022-01-24 13:48:33.445168'
                }
                ]
            }


        # act
        timespan_response = timespan_response_serializer.deserialize(json)

        # assert
        self.assertEqual(datetime(2022, 1, 24, hour=14, minute=28, second=32, microsecond=872532, tzinfo=timezone.utc), timespan_response.request_time)
        self.assertEqual(2, len(timespan_response.timespans))
        self.assertEqual("5bacb7f116e04db2b9aae07bbf9d6493", timespan_response.timespans[0].id.hex)
        self.assertEqual(datetime(2022, 1, 24, hour=13, minute=34, second=28, tzinfo=timezone.utc), timespan_response.timespans[0].start_time)
        self.assertEqual(datetime(2022, 1, 24, hour=13,  minute=34, second=35, tzinfo=timezone.utc), timespan_response.timespans[0].stop_time)
        self.assertEqual("abe433c5724c442eb3cd3ded93b0563e", timespan_response.timespans[1].id.hex)
        self.assertEqual(datetime(2022, 1, 24, hour=13, minute=48, second=31, tzinfo=timezone.utc), timespan_response.timespans[1].start_time)
        self.assertEqual(datetime(2022, 1, 24, hour=13, minute=51, second=42, tzinfo=timezone.utc), timespan_response.timespans[1].stop_time)

if __name__ == "__main__":
    collect()
    unittest.main()
